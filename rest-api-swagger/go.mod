module apiserver

go 1.16

require (
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.28
	go.mongodb.org/mongo-driver v1.5.1
	google.golang.org/grpc v1.37.0
)
