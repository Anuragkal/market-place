package main

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"apiserver/pkg/swagger/server/models"
	"apiserver/pkg/swagger/server/restapi"
	"apiserver/pkg/swagger/server/restapi/operations"
	"apiserver/pkg/swagger/server/restapi/operations/product"
	"apiserver/pkg/swagger/server/restapi/operations/user"
	"apiserver/product_service_api"
	"apiserver/user_service_api"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime/middleware"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
)

const (
	useraddress    = "user-service:8000"
	productaddress = "product-service:8001"
)

func GetLogin(u user.GetLoginParams) middleware.Responder {
	up := strings.Split(u.Authorization, ":")
	username := up[0]
	password := up[1]
	log.Printf("Inside login about to make grpc connection")
	conn, err := connectgrpc(useraddress)
	log.Printf("Inside login about to completed grpc connection")
	if err != nil {
		return user.NewGetLoginBadRequest().WithPayload(err.Error())
	}
	defer disconnect(conn)
	c := user_service_api.NewUserserviceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	result, error := c.Login(ctx, &user_service_api.Creden{Username: username, Password: password})
	if error != nil || result.Name == "nil" {
		log.Printf("Login Failed: %v", error)
		return user.NewGetLoginBadRequest().WithPayload("Failed")
	}
	return user.NewGetLoginOK().WithPayload(&models.LoginResp{Name: &result.Name, Username: &result.Username})
}

func PostSignup(u user.PostSignupParams) middleware.Responder {
	conn, err := connectgrpc(useraddress)
	if err != nil {
		return user.NewPostSignupBadRequest().WithPayload(err.Error())
	}
	defer disconnect(conn)
	c := user_service_api.NewUserserviceClient(conn)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	defer cancel1()
	contact, _ := strconv.Atoi(*u.Data.Contact)
	contact2 := int64(contact)
	r, err := c.AddUser(ctx1, &user_service_api.User{Name: *u.Data.Name, Username: *u.Data.Username, Password: *u.Data.Password, Contact: contact2})
	if err != nil {
		log.Printf("could not add user: %v", err)
		return user.NewPostSignupBadRequest().WithPayload("Failed")
	}
	log.Printf("Adding the user was %s", r.GetStatus())

	return user.NewPostSignupOK().WithPayload("Success")
}

func PostAddProduct(p product.PostAddproductParams) middleware.Responder {
	conn, err := connectgrpc(productaddress)
	if err != nil {
		return product.NewPostAddproductBadRequest().WithPayload(err.Error())
	}
	defer disconnect(conn)
	c := product_service_api.NewProductClient(conn)
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	defer cancel1()
	price, _ := strconv.ParseFloat(*p.Data.Price, 32)
	price2 := float32(price)
	contact, _ := strconv.Atoi(*p.Data.Price)
	contact2 := int64(contact)
	r, err := c.AddProduct(ctx1, &product_service_api.ProductDetails{Name: *p.Data.Name, Price: price2, SellerName: *p.Data.SellerName, SellerUsername: *&p.Username, Contact: contact2, Location: *p.Data.Location, Availability: "True", ProductImg: *p.Data.ProductImg, ProductID: primitive.NewObjectID().String()})
	if err != nil {
		log.Printf("could not insert product data: %v", err)
		return product.NewPostAddproductBadRequest().WithPayload(err.Error())
	}
	log.Printf("Inserting the product data was %s", r.GetCode())
	return product.NewPostAddproductOK().WithPayload(r.GetCode())

}

func GetMyproducts(p product.GetMyproductsParams) middleware.Responder {
	conn, err := connectgrpc(productaddress)
	if err != nil {
		return product.NewGetMyproductsBadRequest().WithPayload(err.Error())
	}
	defer disconnect(conn)
	c := product_service_api.NewProductClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	result, error := c.MyProducts(ctx, &product_service_api.Username{Username: p.Username})
	if error != nil {
		log.Printf("could not get product info: %v", error)
		return product.NewGetMyproductsBadRequest().WithPayload(err.Error())
	}
	var res []*models.ProductListItems0
	for _, v := range result.Res {
		r := &models.ProductListItems0{
			ID:           v.ProductID,
			Name:         v.Name,
			Price:        fmt.Sprintf("%f", v.Price),
			SellerName:   v.SellerName,
			Contact:      strconv.Itoa(int(v.Contact)),
			Location:     v.Location,
			Availability: v.Availability,
			ProductImg:   v.ProductImg,
		}
		res = append(res, r)
	}
	return product.NewGetMyproductsOK().WithPayload(res)
}

func GetBrowseproducts(p product.GetBrowseproductsParams) middleware.Responder {
	conn, err := connectgrpc(productaddress)
	if err != nil {
		return product.NewGetBrowseproductsBadRequest().WithPayload(err.Error())
	}
	defer disconnect(conn)
	c := product_service_api.NewProductClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	result, error := c.BrowseProduct(ctx, &product_service_api.ProductData{Location: *p.Loc, Name: *p.Name})
	if error != nil {
		log.Printf("could not get product info: %v", error)
		return product.NewGetBrowseproductsBadRequest().WithPayload(error.Error())
	}
	var res []*models.ProductListItems0
	for _, v := range result.Res {
		log.Printf(v.ProductID)
		r := &models.ProductListItems0{
			ID:           v.ProductID,
			Name:         v.Name,
			Price:        fmt.Sprintf("%f", v.Price),
			SellerName:   v.SellerName,
			Contact:      strconv.Itoa(int(v.Contact)),
			Location:     v.Location,
			Availability: v.Availability,
			ProductImg:   v.ProductImg,
		}
		res = append(res, r)
	}
	return product.NewGetBrowseproductsOK().WithPayload(res)
}

func PostSell(pid product.PostSellParams) middleware.Responder {
	conn, err := connectgrpc(productaddress)
	if err != nil {
		return product.NewPostSellBadRequest().WithPayload(err.Error())
	}
	defer disconnect(conn)
	c := product_service_api.NewProductClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	id := strings.Split(pid.ID, "\\")
	log.Printf(id[0])
	ID := strings.Join(id, "")
	log.Printf(ID)
	p, err := c.SoldProduct(ctx, &product_service_api.ProductId{Id: ID})
	if err != nil {
		log.Printf("could not sell the product: %v", err)
		return product.NewPostSellBadRequest().WithPayload(err.Error())
	}
	log.Printf("Selling the product data was %s", p.GetCode())
	return product.NewPostSellBadRequest().WithPayload(p.GetCode())
}

func GetCheck(u user.GetCheckusernameParams) middleware.Responder {
	conn, err := connectgrpc(useraddress)
	if err != nil {
		return user.NewGetCheckusernameBadRequest().WithPayload(err.Error())
	}
	defer disconnect(conn)
	c := user_service_api.NewUserserviceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.CheckUsername(ctx, &user_service_api.Username{Username: u.Username})
	if err != nil {
		return user.NewGetCheckusernameOK().WithPayload("Success")
	}

	return user.NewGetCheckusernameBadRequest().WithPayload(r.GetStatus())

}

func main() {

	// Initialize Swagger
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	api := operations.NewAPIServerAPI(swaggerSpec)
	server := restapi.NewServer(api)

	// Implement the GetHelloUser handler
	api.UserGetLoginHandler = user.GetLoginHandlerFunc(GetLogin)
	api.UserPostSignupHandler = user.PostSignupHandlerFunc(PostSignup)
	api.ProductPostAddproductHandler = product.PostAddproductHandlerFunc(PostAddProduct)
	api.ProductGetMyproductsHandler = product.GetMyproductsHandlerFunc(GetMyproducts)
	api.ProductGetBrowseproductsHandler = product.GetBrowseproductsHandlerFunc(GetBrowseproducts)
	api.ProductPostSellHandler = product.PostSellHandlerFunc(PostSell)
	api.UserGetCheckusernameHandler = user.GetCheckusernameHandlerFunc(GetCheck)
	defer func() {
		if err := server.Shutdown(); err != nil {
			// error handle
			log.Fatalln(err)
		}
	}()

	server.Port = 8080
	// Start server which listening
	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}

func connectgrpc(address string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	return conn, err
}
func disconnect(conn *grpc.ClientConn) {
	conn.Close()
}
