// Package main imlements a client for movieinfo service
package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"apiserver/product_service_api"

	"google.golang.org/grpc"
)

const (
	address = "https://35.202.151.48:8001"
)

//func (v *marketapi.ProductDetails) String() string { return fmt.Sprintf(v.ProductName) }

func main() {
	// Set up a connection to the server.
	log.Printf("Going to connect to " + address)
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	log.Printf("connected")
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := product_service_api.NewProductClient(conn)

	// Contact the server and print out its response.
	name := "Furniture"
	var price float32 = 20.5
	sellername := "John Doe"
	sellerusername := "jd23"
	var contact int64 = 8888888888
	location := "Charlotte"
	availability := "True"
	productimg := "tada"
	var productID string = "23"

	// Timeout if server doesn't respond
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	r, err := c.AddProduct(ctx1, &product_service_api.ProductDetails{Name: name, Price: price, SellerName: sellername, SellerUsername: sellerusername, Contact: contact, Location: location, Availability: availability, ProductImg: productimg, ProductID: productID})
	if err != nil {
		log.Fatalf("could not insert product data: %v", err)
	}
	log.Printf("Inserting the product data was %s", r.GetCode())
	cancel1()

	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second)
	result, error := c.BrowseProduct(ctx2, &product_service_api.ProductData{Name: name, Location: location})
	if error != nil {
		log.Fatalf("could not get product info: %v", error)

	}
	for _, v := range result.Res {
		fmt.Println(v)
	}
	cancel2()

	ctx5, cancel5 := context.WithTimeout(context.Background(), time.Second)
	result, error = c.MyProducts(ctx5, &product_service_api.Username{Username: sellerusername})
	if error != nil {
		log.Fatalf("could not get product info: %v", error)

	}
	for _, v := range result.Res {
		fmt.Println(v)
	}
	cancel5()

	ctx3, cancel3 := context.WithTimeout(context.Background(), time.Second)
	p, err1 := c.SoldProduct(ctx3, &product_service_api.ProductId{Id: productID})
	if err1 != nil {
		log.Fatalf("could not sell the product: %v", err1)
	}
	log.Printf("Selling the product data was %s", p.GetCode())
	cancel3()

	ctx4, cancel4 := context.WithTimeout(context.Background(), time.Second)
	res, error := c.BrowseProduct(ctx4, &product_service_api.ProductData{Name: name, Location: location})
	if error != nil {
		log.Fatalf("could not get product info: %v", error)

	}
	for _, v := range res.Res {
		fmt.Println(v)
	}
	cancel4()
}
